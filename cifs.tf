data "http" "json_ip_ranges" {
    url = "https://ip-ranges.atlassian.com/"

    # Optional request headers
    request_headers {
       "Accept" = "application/json"
    }
}

output "cidrs" {
    value = "${lookup(data.http.json_ip_ranges.body, "cidr")}"
}